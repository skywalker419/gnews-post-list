import * as React from 'react';
import axios from "axios";
import { useQuery } from "react-query";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';

import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';

interface NewsPost {
    id: string;
    liked_count: number;
    title: string;
    author: string;
    summary: string;
    avator_img_url: string;
    image_url: string;
}

function NewsPostsList(props:{}) {
    const { data, error, isFetching } = useQuery(['newsPosts'], async () => {
      const { data } = await axios("http://localhost:3000/news-posts");
      return data;
    });
    console.log(data);

    if(isFetching){
      return (
        <div className="App">
              fetching
        </div>
      )
    }

    if(error){
      return (
        <div className="App">
          Error(s) occured.
        </div>
      )
    }

    const likeNewsPost = (newsPostId: string) => async () => {
      // sync call to update backend state
      // use mutation

      // on success, optimistic update frontend states
      // removed disliked status if user disliked this news post
      // update react query reflect the successful updated like state
    };

    const dislikeNewsPost = (newsPostId: string) => async () => {
      // sync call to update backend state
      // use mutation

      // on success, optimistic update frontend states
      // removed disliked status if user disliked this news post
      // update react query reflect the successful updated like state
    };

    const newsPostList = data.map((newsPost:NewsPost) => {
      return (
      <div key={newsPost.id}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Remy Sharp" src={newsPost.avator_img_url} />
        </ListItemAvatar>
        <ListItemText
          sx={{ maxWidth: 545 }}
          primary={newsPost.title}
          secondary={
            <React.Fragment>
              <Typography
                sx={{ display: 'inline' }}
                component="span"
                variant="body2"
                color="text.primary"
              >
                {newsPost.author}
              </Typography>
              <p />
              {newsPost.summary}

            </React.Fragment>
          }
        />
        <Card sx={{ maxWidth: 345 }}>
              <CardMedia
                component="img"
                height="140"
                image={newsPost.image_url}
                alt="green iguana"
              />
              </Card>
      </ListItem>

      <Divider variant="inset" component="li" />
      </div>
      )
    })

    return (
      <List sx={{ width: '100%', maxWidth: 960, bgcolor: 'background.paper' }}>
        {newsPostList}
      </List>
    );
  }

export default NewsPostsList