import './App.css';
import {
  QueryClient,
  QueryClientProvider,
} from 'react-query'
import NewsPostList from './components/news-post-list-comp';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
// Create a client
const queryClient = new QueryClient()

function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}
function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Container maxWidth="md">
      <Box sx={{ my: 4 }}>
        <Typography variant="h4" component="h1" gutterBottom>
          gnews.org demo news post list
        </Typography>
        <NewsPostList />
        <Copyright />
      </Box>
      </Container>
    </QueryClientProvider>
  );
}

export default App;
